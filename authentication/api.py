from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from fvc.models import FinancialVirtualCloudUser


@api_view(['GET'])
def auth_user(request):
    auth_as = None
    user = request.user

    if user.is_authenticated:
        try:
            fvc_object = user.fvc_user.default
            if fvc_object is not None:
                fvc = {
                    'name': fvc_object.name,
                    'id': fvc_object.id
                }
        except FinancialVirtualCloudUser.DoesNotExist:
            fvc = None
        auth_as = {
            'username': user.username,
            'fvc': fvc
        }

    return Response({
        'user': auth_as,
    })


@api_view(['POST'])
def register_user(request):
    username = request.data.get('username')
    password = request.data.get('password')

    user_model = get_user_model()
    if user_model.objects.filter(username__iexact=username).exists():
        return Response({
            'success': False,
            'reason': 'Notandi með netfangið %s er nú þegar til' % username
        }, status=status.HTTP_409_CONFLICT)

    try:
        validate_password(password)
    except ValidationError:
        return Response({
            'success': False,
            'reason': 'Lykilorð er ekki nógu sterkt. '
                      'Það þarf að vera að lágmarki 8 stafir, '
                      "má ekki vera algengt lykilorð á borð við 'password' og "
                      'má ekki vera alfarið samansett af tölustöfum.'
        }, status=status.HTTP_400_BAD_REQUEST)

    user = user_model.objects.create(
        username=username,
        email=username,
    )
    user.set_password(password)
    user.save()
    return Response(status=status.HTTP_201_CREATED)

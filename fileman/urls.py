from django.urls import path, include
from fileman import views

urlpatterns = [
    path('upload/', views.FileUploadView.as_view(), name='file-upload'),
]

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser, MultiPartParser, FormParser
from rest_framework.exceptions import ParseError
from rest_framework import status
from .utils import upload_file
from .models import FileObj
from .serializers import FileObjSerializer
import random

class FileUploadView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    # ONLY TESTING
    def put(self, request, format = None):
        if 'file' not in request.data:
            raise ParseError("Empty content")

        file_obj = request.FILES['file']
        print('Reading fileobj1')
        #print(file_obj.read())

        print(file_obj)
        print('test')

        # slugify name in upload_file instead of here TODO?
        key = 'test{}/{}'.format(random.randint(15,1000),str(file_obj))

        upload_file(file_obj, key, str(file_obj), 'jpg')
        return Response(status = 204)

    # test
    def get(self, request, format = None):
        files = FileObj.objects.all()
        print('COUNT: {}'.format(len(files)))
        serialized = FileObjSerializer(files, many=True)

        return Response(serialized.data, status = 200)

'''
class FileUploadView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, *args, **kwargs):
        file_serializer = FileObjSerializer(data = request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
'''
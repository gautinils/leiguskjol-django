import base64

from django.db import models
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill


class Folder(models.Model):
    name = models.CharField(max_length=512)
    key = models.CharField(max_length=1024, unique=True)
    parent = models.ForeignKey('self', null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    '''
    ACCESS STUFF
    def can_read(self, user):
        userFolder = UserFolder.objects.filter(user = user, folder = self)
        if userFolder.exists():
            return userFolder[0].read
        elif self.parent == None:
            return False
        else:
            return self.parent.can_read(user)

    '''

    class Meta:
        unique_together = ('key', 'parent')


def content_file_name(instance, filename):
    return instance.key.lstrip('/')


def content_file_name_smaller(instance, filename):
    return '/'.join(instance.key.split('/')[:-1]).lstrip('/') + '/' + str(filename)


class FileObj(models.Model):
    file_field = models.FileField(upload_to=content_file_name, null=True)
    smaller = models.ImageField(upload_to=content_file_name_smaller, null=True)
    thumb = ImageSpecField(
        source='smaller',
        processors=[ResizeToFill(60, 60)],
        format='JPEG',
        options={'quality': 60}
    )
    img_height = models.PositiveIntegerField(null=True)
    img_width = models.PositiveIntegerField(null=True)
    byte_content = models.TextField(null=True)  # base64 encoded byte object as string
    name = models.CharField(max_length=512)
    ending = models.CharField(max_length=32)
    key = models.CharField(max_length=1024, unique=True)
    folder = models.ForeignKey(Folder, null=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey('users.User', null=True, on_delete=models.SET_NULL)

    @property
    def url(self):
        """ Remove the secure part from the s3 url, so that browsers cache"""
        url = self.file_field.url
        if "?" in url:
            url = "".join(url.split("?")[:-1])
        return url

    @property
    def smallurl(self):
        """ Remove the secure part from the s3 url, so that browsers cache"""
        imgurl = self.smaller.url if self.smaller else self.file_field.url
        if "?" in imgurl:
            imgurl = "".join(imgurl.split("?")[:-1])
        return imgurl

    def url_key(self):
        return self.key.lstrip('/')

    def get_bytes(self):
        if self.byte_content:
            return base64.b64decode(self.byte_content)
        else:
            return base64.b64decode('')

    def get_image_dimensions(self):
        return get_image_dimensions(self.file_field)

    def height(self):
        if self.img_height:
            return self.img_height
        else:
            h = self.get_image_dimensions()[1]
            if type(h) is int:
                self.img_height = h
                self.save()
            return h

    def width(self):
        if self.img_width:
            return self.img_width
        else:
            w = self.get_image_dimensions()[0]
            if type(w) is int:
                self.img_width = w
                self.save()
            return w

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)


class UserFolder(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    folder = models.ForeignKey(Folder, on_delete=models.CASCADE)
    read = models.BooleanField(default=False)  # ATH
    write = models.BooleanField(default=False)  # ATH
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user', 'folder',)


class UserFile(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    file_obj = models.ForeignKey(FileObj, on_delete=models.CASCADE)
    read = models.BooleanField(default=False)  # ATH
    write = models.BooleanField(default=False)  # ATH
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user', 'file_obj',)

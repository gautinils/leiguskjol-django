from .models import FileObj, Folder

from PIL import Image
from base64 import b64encode
from io import BytesIO
from django.core import files
from slugify import slugify
import tempfile
import random
from django.core.files import File


from django.core.files.uploadedfile import TemporaryUploadedFile, SimpleUploadedFile, InMemoryUploadedFile

suffixconvdict = { "jpg": "jpeg"}

def get_thumbnail(image,suffix="png", w=128, h=128):
    suffix = suffix.lower()
    if suffix in suffixconvdict:
        suffix = suffixconvdict[suffix]
    try:
        image.seek(0)
    except:
        pass
    imgbytes = BytesIO(image.read())
    thumbnail = BytesIO()
    smallerimg = Image.open(imgbytes)
    smallerimg.thumbnail((w, h))
    smallerimg.save(thumbnail, suffix)

    return "data:image/"+suffix+";base64,"+b64encode(thumbnail.getvalue()).decode("utf-8")

def get_smaller(image, suffix="png",w=736, h=736):
    suffix = suffix.lower()
    if suffix in suffixconvdict:
        suffix = suffixconvdict[suffix]
    
    try:
        image.seek(0)
    except:
        pass

    imgbytes = BytesIO(image.read())

    thumbnail = BytesIO()
    smallerimg = Image.open(imgbytes)
    smallerimg.thumbnail((w, h))
    smallerimg.save(thumbnail, suffix)
    thumbnail.seek(0)
    return thumbnail


def gen_random_img_name(suffix, instance, filename, bits = 32):
    suffix = suffix.lower()
    name = hex(random.getrandbits(bits))[2:-1] + "." + suffix

    while FileObj.objects.filter(smaller=name).exists():
        name = hex(random.getrandbits(bits))[2:-1] + "." + suffix

    return name


def create_folder(key, parent = None):
    if key == '':
        return parent

    keys = key.split('/')
	
    if keys[0] == '':
        folder = None
    else:
        folder_name = keys[0]
        if parent == None:
            key = folder_name
        else:
            key = parent.key + '/' + folder_name
		
        folder, folderCreated = Folder.objects.get_or_create(parent = parent, key = key)
        folder.name = folder_name
        folder.save()

    return create_folder(key = '/'.join(keys[1:]), parent = folder)


def upload_file(file_obj, key, filename, ending = '', folder = None, created_by = None):
    print('UPLOADING...')
    if folder:
        folderKey = folder.key
    else:
        folderKey = key

    file_obj.name = filename
    fileObj = FileObj(**{'file_field': file_obj, 'name': filename, 'ending':ending, 'key':key})

    if folder:
        fileObj.folder = folder
    if created_by:
        fileObj.created_by = created_by

        
    fileObj.save()
    
    # If image, make a smaller version
    imgEndings = ['jpg','png','gif','jpeg','bmp','JPG','JPEG','PNG','GIF','BMP']
    suffix = ending
    if suffix in imgEndings:
        #generate random available name
        print('Generating random name')
        
        name = gen_random_img_name(suffix, instance=fileObj, filename='')

        print('{} got generated'.format(name))
        smallerimg = get_smaller(fileObj.file_field, suffix)
        print('Smaller img got generated')
        smallerimg = File(smallerimg)
        fileObj.smaller.save(name, smallerimg)
        print('trying to get thumbnail')
        thumb = get_thumbnail(fileObj.file_field, suffix)
        fileObj.thumb = thumb

    return fileObj
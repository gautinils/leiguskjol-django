from rest_framework import serializers
from .models import FileObj


class FileObjSerializer(serializers.ModelSerializer):
    class Meta:
        model = FileObj
        fields = ('file_field', 'smaller', 'created_at')

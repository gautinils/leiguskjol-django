from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from accounting.models import Transaction, Document, DocumentFileObj, LedgerEntry, Account, Ledger, AccountType
from customers.models import Customer
from customers.serializers import CustomerSerializer
from fileman.serializers import FileObjSerializer


class DocumentFileObjSerializer(serializers.ModelSerializer):
    file = FileObjSerializer()
    url = serializers.HyperlinkedIdentityField(read_only=True, view_name='document_file_obj')

    class Meta:
        model = DocumentFileObj
        fields = ['file', 'url']
        read_only_fields = ['url']


class DocumentSerializer(serializers.ModelSerializer):
    files = DocumentFileObjSerializer(many=True, allow_empty=False)

    def create(self, validated_data):
        files = validated_data.pop('files')
        document = Document.objects.create(**validated_data)
        for file in files:
            DocumentFileObj.objects.create(document=document, **file)
        return document

    class Meta:
        model = Document
        fields = ['id', 'name', 'files']
        read_only_fields = ['id']


class AccountEntrySerializer(serializers.ModelSerializer):
    customer = CustomerSerializer(read_only=True)
    date = serializers.DateField(source='transaction.date', read_only=True)
    transaction_client_id = serializers.IntegerField(source='transaction.client_id', read_only=True)

    class Meta:
        model = LedgerEntry
        fields = ['id', 'is_debit', 'amount', 'description', 'customer', 'account', 'date', 'transaction_client_id']


class AccountDetailSerializer(serializers.ModelSerializer):
    entries = AccountEntrySerializer(read_only=True, many=True)
    is_debit = serializers.BooleanField(source='type.is_debit', read_only=True)
    balance = serializers.ReadOnlyField()

    class Meta:
        model = Account
        fields = ['id', 'name', 'ledger', 'type', 'parent', 'is_debit', 'balance', 'entries']
        read_only_fields = ['id', 'is_debit', 'ledger', 'type', 'parent', 'balance', 'entries']
        depth = 1


class AccountSerializer(serializers.ModelSerializer):
    is_debit = serializers.BooleanField(source='type.is_debit', read_only=True)
    ledger_id = serializers.PrimaryKeyRelatedField(queryset=Ledger.objects.all(), write_only=True, source='ledger')
    type_id = serializers.PrimaryKeyRelatedField(queryset=AccountType.objects.all(), write_only=True, source='type')
    parent_id = serializers.PrimaryKeyRelatedField(queryset=Account.objects.all(), required=False, write_only=True,
                                                   source='parent')

    class Meta:
        model = Account
        fields = ['id', 'name', 'ledger', 'type', 'parent', 'is_debit', 'ledger_id', 'type_id',
                  'parent_id']
        read_only_fields = ['id', 'is_debit', 'ledger', 'type', 'parent']
        depth = 1


class AccountTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = AccountType
        fields = ['id', 'is_debit', 'name']


class LedgerEntrySerializer(serializers.ModelSerializer):
    customer = CustomerSerializer(read_only=True)
    customer_id = PrimaryKeyRelatedField(queryset=Customer.objects.all(), write_only=True, source='customer')
    account_id = PrimaryKeyRelatedField(queryset=Account.objects.all(), write_only=True, source='account')

    class Meta:
        model = LedgerEntry
        fields = ['id', 'is_debit', 'amount', 'customer', 'customer_id', 'account', 'account_id']
        read_only_fields = ['id', 'customer_id', 'account_id']
        depth = 1


def validate_equation(entries):
    """
    Validates that the fundamental accounting equation is respected
    """
    total_amount = 0
    for entry in entries:
        increase = entry['is_debit'] == entry['account'].type.is_debit
        total_amount += entry['amount'] if increase else total_amount - entry['amount']
    return total_amount == 0


def validate_account_open(transaction_date, account):
    """
    Validates that an account is open on the transaction date
    """
    return account.closed_date < transaction_date


class TransactionSerializer(serializers.ModelSerializer):
    document = DocumentSerializer(required=False)
    entries = LedgerEntrySerializer(many=True, allow_empty=False)
    fvc = PrimaryKeyRelatedField(read_only=True)

    def validate(self, data):
        entries = data['entries']
        if not validate_equation(entries):
            raise serializers.ValidationError("Entries do not respect the fundamental accounting equation")
        unique_account = all(a['account'].ledger == entries[0]['account'].ledger for a in entries)
        if not unique_account:
            raise serializers.ValidationError("All ledger entries must belong to the same ledger")
        date = data['date']
        for entry in entries:
            if not validate_account_open(date, entry['account']):
                m = "Entries cannot be added to '{}' on the given transaction date".format(entry['account'].name)
                raise serializers.ValidationError(m)
        return data

    def create(self, validated_data):
        entries = validated_data.pop('entries')
        document_data = validated_data.pop('document', None)
        if document_data:
            document = Document.objects.create(fvc=validated_data.get('fvc'), **document_data)
            transaction = Transaction.objects.create(document=document, **validated_data)
        else:
            transaction = Transaction.objects.create(**validated_data)
        for entry in entries:
            LedgerEntry.objects.create(transaction=transaction, **entry)
        return transaction

    class Meta:
        model = Transaction
        fields = ['id', 'fvc', 'description', 'date', 'entries', 'document', 'client_id', 'client_ref']
        read_only_fields = ['id', 'fvc', 'client_id', 'client_ref']


class LedgerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ledger
        fields = ['id', 'name', 'parent']
        read_only_fields = ['id']

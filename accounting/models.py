import uuid

from django.db import models, transaction
from django.db.models import UniqueConstraint, Sum, Q


class Ledger(models.Model):
    name = models.CharField(max_length=40)
    parent = models.ForeignKey('self', null=True, on_delete=models.PROTECT)
    fvc = models.ForeignKey('fvc.FinancialVirtualCloud', on_delete=models.PROTECT)


class AccountType(models.Model):
    name = models.CharField(max_length=40)
    is_debit = models.BooleanField(editable=False)


class Account(models.Model):
    name = models.CharField(max_length=40)
    ledger = models.ForeignKey(Ledger, on_delete=models.PROTECT)
    type = models.ForeignKey(AccountType, on_delete=models.PROTECT)
    parent = models.ForeignKey('self', null=True, on_delete=models.PROTECT)

    def calculate_balance(self, amounts, stored_bal):
        debit = amounts.get('debit') or 0
        credit = amounts.get('credit') or 0
        if self.type.is_debit:
            return stored_bal + debit - credit
        else:
            return stored_bal + credit - debit

    @property
    def balance(self):
        """
        The account's balance is the sum of all its entries
        """
        # Last stored balance
        stored_balance = self.balances.first()
        if stored_balance:
            bal = stored_balance.balance
            amounts = self.entries.aggregate(
                debit=Sum('amount', filter=Q(is_debit=True, transaction__date__gt=stored_balance.date)),
                credit=Sum('amount', filter=Q(is_debit=False, transaction__date__gt=stored_balance.date))
            )
            return self.calculate_balance(amounts, bal)
        else:
            bal = 0
            amounts = self.entries.aggregate(
                debit=Sum('amount', filter=Q(is_debit=True)),
                credit=Sum('amount', filter=Q(is_debit=False))
            )
            return self.calculate_balance(amounts, bal)

    @property
    def closed_date(self):
        """
        The
        """
        stored_balance = self.balances.first()
        if stored_balance:
            return stored_balance.date
        return None


class AccountBalance(models.Model):
    date = models.DateField()
    balance = models.DecimalField(max_digits=14, decimal_places=4, default=0)
    account = models.ForeignKey(Account, related_name='balances', on_delete=models.CASCADE)

    class Meta:
        ordering = ['-date']


class Document(models.Model):
    name = models.CharField(max_length=40, blank=True)
    fvc = models.ForeignKey('fvc.FinancialVirtualCloud', on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey('fvc.FinancialVirtualCloudUser', null=True, on_delete=models.SET_NULL)


class DocumentFileObj(models.Model):
    document = models.ForeignKey(Document, related_name='files', on_delete=models.PROTECT)
    file = models.ForeignKey('fileman.FileObj', on_delete=models.PROTECT)


class Transaction(models.Model):
    fvc = models.ForeignKey('fvc.FinancialVirtualCloud', on_delete=models.PROTECT)
    date = models.DateField()
    description = models.CharField(max_length=255)
    document = models.ForeignKey(Document, on_delete=models.PROTECT, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey('fvc.FinancialVirtualCloudUser', null=True, on_delete=models.SET_NULL)
    client_ref = models.CharField(max_length=255, blank=True)
    client_id = models.IntegerField(default=1)

    class Meta:
        ordering = ['-client_id']
        UniqueConstraint(fields=['fvc', 'client_id'], name='client_id_sequence')

    @transaction.atomic
    def save(self, *args, **kwargs):
        if self._state.adding:
            count = Transaction.objects.filter(fvc=self.fvc).count()
            self.client_id = count + 1
        super(Transaction, self).save(*args, **kwargs)


class LedgerEntry(models.Model):
    transaction = models.ForeignKey(Transaction, on_delete=models.PROTECT, related_name="entries")
    account = models.ForeignKey(Account, related_name='entries', on_delete=models.PROTECT)
    is_debit = models.BooleanField()
    amount = models.DecimalField(max_digits=14, decimal_places=4)
    customer = models.ForeignKey('customers.Customer', blank=True, on_delete=models.PROTECT)
    description = models.CharField(max_length=255, blank=True)

    @property
    def currency(self):
        return self.transaction.fvc.currency

from django.urls import path
from rest_framework import routers

from accounting import api
from accounting.api import TransactionViewSet, AccountViewSet

router = routers.SimpleRouter()
router.register(r'transactions', TransactionViewSet)
router.register(r'accounts', AccountViewSet)

urlpatterns = [
    path('account-types', api.AccountTypeList.as_view()),
] + router.urls

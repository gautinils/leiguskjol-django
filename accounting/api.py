from rest_framework import permissions, mixins, viewsets, generics
from accounting.models import Transaction, AccountType, Account
from accounting.serializers import TransactionSerializer, AccountTypeSerializer, AccountDetailSerializer


class TransactionViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    permission_classes = [permissions.IsAuthenticated, ]

    def get_queryset(self):
        qs = super().get_queryset()
        user = self.request.user
        return (qs.filter(fvc__users__user=user)
                .select_related('document')
                .prefetch_related('entries__account', 'entries__customer'))


class AccountTypeList(generics.ListAPIView):
    queryset = AccountType.objects.all()
    serializer_class = AccountTypeSerializer
    permission_classes = [permissions.IsAuthenticated, ]


class AccountViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountDetailSerializer
    permission_classes = [permissions.IsAuthenticated, ]

    def get_queryset(self):
        qs = super().get_queryset()
        user = self.request.user
        return (qs.filter(ledger__fvc__users__user=user)
                .select_related('ledger')
                .prefetch_related('entries__transaction', 'entries__customer'))

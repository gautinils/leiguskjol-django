# Development:
 * It is recommended to install, create and activate a [Virtual environment](https://virtualenv.pypa.io/en/stable/) for the project
    * Install pip: `apt install python3-pip`
    * Install virtualenv: `python3 -m pip install virtualenv`
    * Create an env: `python3 -m virtualenv env`
    * Activate the env: `source env/bin/activate`
 * Install dependencies: `pip install -r requirements.txt`
 * Create local_settings.py and set the following variables:
  * `SECRET_KEY` 
  * `DATABASES`  
 * To run the development server: `python manage.py runserver`


# Production:
 * Make sure you have [Docker](https://www.docker.com) and [Docker compose](https://docs.docker.com/compose/) installed and the Docker daemon running
 * The database data is stored in `/postgres-data` which is mounted as a volume in the database container, make sure this directory exists
 * Create the .env file and set the following variables:
   * `SECRET_KEY` (you can [generate](http://www.miniwebtool.com/django-secret-key-generator/) one)
   * `POSTGRES_DB`
   * `POSTGRES_USER`
   * `POSTGRES_PASSWORD`
 * Run `docker-compose up`
 * This runs a [gunicorn](https://gunicorn.org/) server ready to be proxied behind [nginx](https://nginx.org/en/).
 * Run `docker-compose exec web python manage.py migrate`
from django.db import models

from leiguskjol import settings


class Currency(models.Model):
    quote = models.CharField(max_length=3)
    symbol = models.CharField(max_length=8, null=True)
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.quote


class FinancialVirtualCloud(models.Model):
    name = models.CharField(max_length=40)
    currency = models.ForeignKey(Currency, on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class FinancialVirtualCloudUser(models.Model):
    financial_virtual_clouds = models.ManyToManyField(FinancialVirtualCloud, related_name='users')
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='fvc_user', on_delete=models.CASCADE)
    default = models.ForeignKey(FinancialVirtualCloud, on_delete=models.PROTECT)

    def __str__(self):
        return str(self.user)

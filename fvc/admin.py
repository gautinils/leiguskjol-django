from django.contrib import admin

from fvc.models import FinancialVirtualCloud, FinancialVirtualCloudUser


class FinancialVirtualCloudAdmin(admin.ModelAdmin):
    fields = ['name']


class FinancialVirtualCloudUserAdmin(admin.ModelAdmin):
    fields = ['user', 'financial_virtual_clouds']


admin.site.register(FinancialVirtualCloud, FinancialVirtualCloudAdmin)
admin.site.register(FinancialVirtualCloudUser, FinancialVirtualCloudUserAdmin)

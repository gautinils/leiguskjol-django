from rest_framework import serializers

from fvc.models import FinancialVirtualCloud, FinancialVirtualCloudUser, Currency


class FinancialVirtualCloudSerializer(serializers.ModelSerializer):
    class Meta:
        model = FinancialVirtualCloud
        fields = ['id', 'name']

    def create(self, validated_data):
        fvc = FinancialVirtualCloud.objects.create(**validated_data)
        user = self.context['request'].user
        try:
            fvc_user = FinancialVirtualCloudUser.objects.get(user=user)
        except FinancialVirtualCloudUser.DoesNotExist:
            fvc_user = FinancialVirtualCloudUser.objects.create(user=user, default=fvc)
        fvc_user.financial_virtual_clouds.add(fvc)
        return fvc


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = ['id', 'quote', 'title']
        read_only_fields = ['id']

from django.urls import path

from fvc import api

urlpatterns = [
    path('fvc/', api.FVCListCreate.as_view()),
    path('fvc/<int:pk>/transactions/', api.TransactionListCreate.as_view()),
    path('fvc/<int:pk>/documents/', api.DocumentListCreate.as_view()),
    path('fvc/<int:pk>/customers/', api.CustomerListCreate.as_view()),
    path('fvc/<int:pk>/ledgers/', api.LedgerListCreate.as_view()),
    path('fvc/<int:pk>/accounts/', api.AccountListCreate.as_view()),
    path('currencies/', api.CurrencyList.as_view())
]

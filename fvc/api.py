from django.http import Http404
from rest_framework import generics, permissions, status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from accounting.models import Transaction, Document, Ledger, Account
from accounting.serializers import TransactionSerializer, DocumentSerializer, LedgerSerializer, AccountSerializer
from customers.models import Customer
from customers.serializers import CustomerSerializer
from fvc.models import FinancialVirtualCloud, Currency
from fvc.serializers import FinancialVirtualCloudSerializer, CurrencySerializer


def get_fvc_or_404(pk, user):
    fvc = get_object_or_404(FinancialVirtualCloud, pk=pk)
    if fvc.users.filter(user=user).exists():
        return fvc
    else:
        # FIXME 401 error
        raise Http404


class FVCListCreate(generics.ListCreateAPIView):
    queryset = FinancialVirtualCloud.objects.all()
    serializer_class = FinancialVirtualCloudSerializer
    permission_classes = [permissions.IsAuthenticated, ]

    def get_queryset(self):
        user = self.request.user
        return super().get_queryset().filter(users__user=user)

    def list(self, request, *args, **kwargs):
        qs = self.get_queryset()
        serializer = FinancialVirtualCloudSerializer(qs, many=True)
        return Response(serializer.data)


class TransactionListCreate(generics.ListCreateAPIView):
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        fvc_pk = self.kwargs['pk']
        return (super().get_queryset()
                .filter(fvc__id=fvc_pk)
                .prefetch_related('entries__customer', 'entries__account', 'document__files'))

    def list(self, request, *args, **kwargs):
        # Check if user has access to the fvc
        get_fvc_or_404(pk=self.kwargs["pk"], user=request.user)
        transactions = self.get_queryset()
        serializer = TransactionSerializer(transactions, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        # Check if user has access to the fvc
        fvc = get_fvc_or_404(pk=self.kwargs["pk"], user=request.user)
        self.check_object_permissions(self.request, fvc)
        serializer = TransactionSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=request.user.fvc_user, fvc=fvc)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class DocumentListCreate(generics.ListCreateAPIView):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        fvc_pk = self.kwargs['pk']
        return (super().get_queryset()
                .filter(fvc__id=fvc_pk)
                .prefetch_related('files'))

    def list(self, request, *args, **kwargs):
        # Check if user has access to the fvc
        get_fvc_or_404(pk=self.kwargs["pk"], user=request.user)
        documents = self.get_queryset()
        serializer = DocumentSerializer(documents, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        # Check if user has access to the fvc
        fvc = get_fvc_or_404(pk=self.kwargs["pk"], user=request.user)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=request.user.fvc_user, fvc=fvc)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class CustomerListCreate(generics.ListCreateAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        fvc_pk = self.kwargs['pk']
        qs = super().get_queryset().filter(fvc__id=fvc_pk)
        query = self.request.query_params.get('q', None)
        if query:
            qs = qs.filter(name__icontains=query)
        return qs

    def list(self, request, *args, **kwargs):
        # Check if user has access to the fvc
        get_fvc_or_404(pk=self.kwargs["pk"], user=request.user)
        customers = self.get_queryset()
        serializer = self.get_serializer(customers, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        fvc = get_fvc_or_404(pk=self.kwargs["pk"], user=request.user)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(fvc=fvc)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class LedgerListCreate(generics.ListCreateAPIView):
    queryset = Ledger.objects.all()
    serializer_class = LedgerSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        fvc_pk = self.kwargs['pk']
        return (super().get_queryset()
                .filter(fvc__id=fvc_pk))

    def list(self, request, *args, **kwargs):
        # Check if user has access to the fvc
        get_fvc_or_404(pk=self.kwargs["pk"], user=request.user)
        ledgers = self.get_queryset()
        serializer = self.get_serializer(ledgers, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        # Check if user has access to the fvc
        fvc = get_object_or_404(FinancialVirtualCloud, pk=self.kwargs["pk"])
        self.check_object_permissions(self.request, fvc)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(fvc=fvc)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class AccountListCreate(generics.ListCreateAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        fvc_pk = self.kwargs['pk']
        qs = (super().get_queryset()
              .filter(ledger__fvc__id=fvc_pk))
        ledger = self.request.query_params.get('ledger', None)
        if ledger:
            qs = qs.filter(ledger__id=ledger)
        query = self.request.query_params.get('q', None)
        if query:
            qs = qs.filter(name__icontains=query)
        return qs

    def list(self, request, *args, **kwargs):
        get_fvc_or_404(pk=self.kwargs["pk"], user=request.user)
        accounts = self.get_queryset()
        serializer = AccountSerializer(accounts, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        # Check if user has access to the fvc
        get_fvc_or_404(pk=self.kwargs["pk"], user=request.user)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class CurrencyList(generics.ListAPIView):
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer
    permission_classes = [permissions.IsAuthenticated, ]
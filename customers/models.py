from django.db import models


class Customer(models.Model):
    name = models.CharField(max_length=120)
    fvc = models.ForeignKey('fvc.FinancialVirtualCloud', on_delete=models.PROTECT)

    def __str__(self):
        return self.name
